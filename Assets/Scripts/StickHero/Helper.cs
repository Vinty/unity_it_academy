﻿using System;
using System.Threading;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using Random = UnityEngine.Random;

namespace StickHero
{
    public class Helper : MonoBehaviour
    {
        public static void SetMaterial(GameObject obj)
        {
            var newMat = Resources.Load("FlatRocks", typeof(Material)) as Material;
            var tex = Resources.Load("Barriers") as Texture;
            var mRenderer = obj.GetComponent<Renderer>();
            if (newMat == null) return;
            newMat.mainTexture = tex;
            mRenderer.material = newMat;
        }

        public static float GetRandomLenght()
        {
            var _a = Random.Range(1, 4);
            var _b = Random.Range(4, 6);
            return Random.Range(_a, _b);
        }
    }
}
