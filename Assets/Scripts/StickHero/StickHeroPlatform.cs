﻿using System;
using System.Collections;
using System.Collections.Generic;
using StickHero;
using UnityEngine;

public class StickHeroPlatform : MonoBehaviour
{
    private static Component _curObj = new Component();
    
    private GameObject _nextPlatform;
    private const float StartPoint = 3f;
    private float _deltaLenght = 0f;
    
    private void Start()
    {
        _curObj = gameObject.GetComponent<Component>();
    }

    private void Update()
    {
//        if (!_curObj.gameObject.Equals(_nextPlatform))
//        {
//            _nextPlatform = _curObj.gameObject;
//        }
    }

    public Transform m_StickPoint;

    public Vector3 GetStickPosition()
    {
        return m_StickPoint.position;
    }

    public float GetPlatformSize()
    {
        return transform.localScale.x;
    }

    public List<GameObject> GetRandomListPlatforms()
    {
        var list = new List<GameObject>();

        return list;
    }

//    public static GameObject GetRandomPlatform()
//    {
//        GameObject curPlatform = Instantiate(curObj.gameObject);
//        return curPlatform;
//    }

    public void SetNextPlatform(GameObject currentPlatform)
    {
        _nextPlatform = currentPlatform;
    }
    
    public GameObject GetRandomPlatform()
    {
        _nextPlatform = Instantiate(gameObject);
        Transform nextTransform = GetNextPlatformTransform(_nextPlatform);
        _nextPlatform.transform.position = nextTransform.position;
        _nextPlatform.transform.localScale = nextTransform.localScale;
        return _nextPlatform;
    }
    public Transform GetNextPlatformTransform(GameObject nextPlatform)
    {
        var positionX = nextPlatform.transform.position.x;
        var positionY = nextPlatform.transform.position.y;
        var positionZ = nextPlatform.transform.position.z;
        
        var localScaleY = nextPlatform.transform.localScale.y;
        var localScaleZ = nextPlatform.transform.localScale.z;

        var lenghtPlatform = 0.2f * Helper.GetRandomLenght() + 0.2f;
        var minSpaceLenght = 0.2f;
        _deltaLenght += 0.1f * Helper.GetRandomLenght() + lenghtPlatform + minSpaceLenght;

        nextPlatform.transform.position = new Vector3(
            positionX = StartPoint + _deltaLenght, 
            positionY, 
            positionZ 
            );
        nextPlatform.transform.localScale = new Vector3(
            lenghtPlatform, localScaleY, localScaleZ
            );
        return nextPlatform.transform;
    }
    
    
//    public GameObject GetCopyOfThis()
//    {
//        return Instantiate(gameObject);
//    }
}
