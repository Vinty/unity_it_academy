﻿using System.Collections;
using System.Collections.Generic;
using StickHero;
using UnityEngine;
using UnityEngine.Serialization;

public class StickHeroStick : MonoBehaviour
{
    [SerializeField] private StickHeroController m_Controller;
    [SerializeField] private float speedStickScale = 0.5f;
    [SerializeField] private float speedStickRotate = 90f;
    [SerializeField] private GameObject stickMesh;
    private bool isScale;
    private bool isRotate;
    private float angleZ;
    
    
    void Start()
    {
        Helper.SetMaterial(stickMesh);
    }

    // Update is called once per frame
    private void Update()
    {
        if (isScale)
        {
            float scaleY = transform.localScale.y;
            if (scaleY >= 2f)
            {
                StopScaling();
                return;
            }

            scaleY += Time.deltaTime * speedStickScale;
            transform.localScale = new Vector3(transform.localScale.x, 
                scaleY, transform.localScale.z);
        }

        if (!isRotate)
            return;

        //float angle = Vector3.Angle(transform.up, Vector3.right);
        if (angleZ <= -90f)
        {
            StopRotate();
            return;
        }

        angleZ -= Time.deltaTime * speedStickRotate;
        transform.localEulerAngles = new Vector3(0f, 0f, angleZ);
        
    }

    public void ResetStick(Vector3 newPosition)
    {
        transform.position = newPosition;
        Vector3 scale = new Vector3(1f, 0, 1f);
        transform.localScale = scale;
        
        transform.localEulerAngles = Vector3.zero;
    }

    public void StartScaling()
    {
        isScale = true;
    }

    public void StartRotate()
    {
        isRotate = true;
    }

    public void StopScaling()
    {
        isScale = false;
        angleZ = 0f;
        m_Controller.StopStickScale();
    }

    public void StopRotate()
    {
        isRotate = false;
        transform.localEulerAngles = new Vector3(0f, 0f, -90f);
        
        m_Controller.OnStickStopRotation(transform.localScale.y);
    }
}
