﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StickHeroController : MonoBehaviour
{
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private StickHeroPlatform[] m_Platforms;
    private LinkedList<GameObject> _platformsList = new LinkedList<GameObject>();
    [SerializeField] private StickHeroPlatform _stickHeroPlatform;
    [SerializeField] private GameObject textScore; 

    private int counter;//счетчик платформ
    private int listCounter;
    private bool _startList = false;
    StickHeroPlatform nextPlatform;
    private int score = 0;
    
    public enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeat,
        BuildPlatforms
    }

    private EGameState currentGameState;
    
    private void Start()
    {
        currentGameState = EGameState.Wait;
        counter = 0;
        listCounter = 0;
        
        //ставим стик в точку старта
        m_Stick.ResetStick(m_Platforms[0].GetStickPosition());
    }

    // Update is called once per frame
    private void Update()
    {
        // всегда иметь в запасе 5 платформ в связанном списке
        if (listCounter >= 0 && listCounter < 6)
        {
            // Платформа будет создана по образу и подобию первой платформы (мы дали ее на вход)
            // но с рандомной длиной. Идущие друг за другом они в списке
            // уже разделены случайными промежутками
            var randomPlatform = _stickHeroPlatform.GetRandomPlatform();
            randomPlatform.gameObject.name = "RandomPlatform_" + listCounter;
            // платформа помещается всегда в конец списка, а удаляется позже сначала списка. Правило - FIFO
            _platformsList.AddLast(randomPlatform);
            listCounter++;
        }
            
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }

        switch (currentGameState)
        {
            case EGameState.Wait:
                currentGameState = EGameState.Scaling;
                m_Stick.StartScaling();
                break;
            case EGameState.Scaling:
                currentGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                break;
            
            case EGameState.Rotate:
                break;
            
            case EGameState.Movement:
                break;
            
            case EGameState.Defeat:
                print("Game restarted");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
//            case EGameState.B 
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void StopStickScale()
    {
        currentGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }

    /// <summary>
    /// Начинаем движение персонажа
    /// </summary>
    /// <param name="stickLenght">длина платформы с которой начинаем движение</param>
    public void OnStickStopRotation(float stickLenght)
    {
        currentGameState = EGameState.Movement;
        
        if (counter + 1 < m_Platforms.Length)
        {
            nextPlatform = m_Platforms[counter + 1];
        }
        else
        { 
            var platformsListFirst = _platformsList.First;
            nextPlatform = platformsListFirst.Value.GetComponent<StickHeroPlatform>();
//            listCounter--;
            _startList = true;

        }
        
        //находим минимальную длину стика для перехода
        float targetLenght = nextPlatform.transform.position.x - m_Stick.transform.position.x;
        float platformSize = nextPlatform.GetPlatformSize();

        float min = targetLenght - platformSize * 0.5f;

        min -= m_Player.transform.localScale.x * 0.9f;
        
        //находим максимальное расстояние для перехода
        float max = targetLenght + platformSize * 0.5f;
//        Debug.LogError($"stick {stickLenght} min {min} max {max}");
        //при успехе переходим в центр платформы, иначе падаем
        if (stickLenght < min || stickLenght > max)
        {
            //падаем
            var stickPosition = m_Stick.transform.position.x;
            var halfPlayerSize = m_Player.transform.localScale.x * 0.5f;
            float targetPosition = stickPosition + stickLenght + halfPlayerSize;
            m_Player.StartMovement(targetPosition, true);
        }
        else
        {
            // нормальное движение 
            float targetPosition = nextPlatform.transform.position.x;
            m_Player.StartMovement(targetPosition, false);
            if (_startList)
            {
                _platformsList.RemoveFirst();
                listCounter--;
            }
            score++;  
            textScore.GetComponent<TextMeshPro>().SetText(score.ToString());
        }
    }

    public void OnPlayerStop()
    {
        currentGameState = EGameState.Wait;
        counter++;
        if (counter < 3)
        {
            m_Stick.ResetStick(m_Platforms[counter].GetStickPosition());    
        }
        else
        {
            m_Stick.ResetStick(nextPlatform.GetStickPosition());
        }
    }

    public void OnPlayerDie()
    {
        currentGameState = EGameState.Defeat;
        print("Game Over");
    }
}
