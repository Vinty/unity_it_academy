﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Hop;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private GameObject m_Platform;
    private  List<GameObject> platforms = new List<GameObject>();
    private int _currentIndexPlatform = 1;

    void Start()
    {
        platforms.Add(m_Platform);
        for (int i = 0; i < 25; i++)
        {
            GameObject obj = Instantiate(m_Platform, transform);
            
            var position = Vector3.zero;
            position.z = (i);
            position.x = 0;
            obj.transform.position = position;
            obj.name = $"Platform_{i}";
          
            platforms.Add(obj);
        }
    }

    // TODO сделать метод
    public bool IsBallOnPlatform(Vector3 position)
    {
        position.y = 0f;
        // TODO здесь  можно считать и по номерам другим способом
        GameObject nearestPlatform = platforms[0];
        // начальное значение будет уменьшаться
        // на величину уже бывших в употреблении платформ
        for (int i = _currentIndexPlatform, count =  _currentIndexPlatform + 4; i < count; i++)
        {
            if (platforms[i].transform.position.z + 0.5f < position.z)
            {
                continue;
            }
            if (platforms[i].transform.position.z - position.z > 1f)
            {
                continue;
            }
            nearestPlatform = platforms[i];
            _currentIndexPlatform++;
       
            break;
        }
        float minX = nearestPlatform.transform.position.x - 0.5f;
        float maxX = nearestPlatform.transform.position.x + 0.5f;
        return position.x > minX && position.x < maxX;
    }
}
