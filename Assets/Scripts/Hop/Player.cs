﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    [SerializeField] private AnimationCurve m_JumpCurve;
    // высота прыжка
    [SerializeField] private float m_JumpHeight = 0f;
    // длина прыжка
    [SerializeField] private float m_JumpDistance = 0f;
    // скорость в метрах в секунду - или 1 юнит в секунду
    [SerializeField] private float m_BallSpeed = 1f;
    [SerializeField] private HopInput m_Input;
    [SerializeField] private HopTrack m_Track;

    [SerializeField] private float heightForJumpPlatform = 0f;
    [SerializeField] private float speedForJumpPlatform = 1.2f;

    private bool _isJumpPlatform;

    private float iteration; // цикл прыжка
    private float startZ; // точка начала прыжка
    
    void Update()
    {
        var position = transform.position;
        // смещение
        position.x = Mathf.Lerp(position.x, m_Input.Strafe, Time.deltaTime);
        // прыжок
        position.y = 0;
        
        // движение вперед
        position.z +=  Time.deltaTime * m_BallSpeed;

        transform.position = position;
        // увеличиваем счетчик прыжка
        iteration += Time.deltaTime * m_BallSpeed;
       

        iteration = 0f;
        startZ += m_JumpDistance;
       
        //иначе делаем проигрыш
      
      
        
    }

    private void SetDefaultFields()
    {
        m_JumpHeight = 1f;
        m_BallSpeed = 1f;
    }
    private void SetDefaultJumpDistance()
    {
        m_JumpDistance = 2f;
    }
}
