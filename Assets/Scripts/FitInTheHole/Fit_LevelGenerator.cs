﻿using System.Collections;
using System.Collections.Generic;
using FitInTheHole;
using UnityEngine;

public class Fit_LevelGenerator : MonoBehaviour{
    [SerializeField] private GameObject     m_CubePrefab;
    [SerializeField] private float          m_BaseSpeed    = 10f;
    [SerializeField] private float          m_WallDistance = 35;
    [SerializeField] private Fit_Template[] m_TemplatesPrefabs; // 

    private Fit_Wall       wall;
    private Fit_Template[] templates;
    private Fit_Template   figure; // текущая фигура
    private float          speed;

    private void Start() {
        templates = new Fit_Template[m_TemplatesPrefabs.Length];
        for(int i = 0; i < templates.Length; i++) {
            templates[i] = Instantiate(m_TemplatesPrefabs[i]);
            templates[i].gameObject.SetActive(false);
            templates[i].transform.position = new Vector3(0f, 0.5f, -m_WallDistance);
        }

        speed = m_BaseSpeed;
        wall  = new Fit_Wall(5, 5, m_CubePrefab);
        setupTemplate();
        wall.SetUpWall(figure, m_WallDistance);

        
    }

    private void Update() {
        wall.Parent.Translate(Time.deltaTime * speed * Vector3.back);
        if(wall.Parent.position.z > m_WallDistance * -1) { return; }

        setupTemplate();
        wall.SetUpWall(figure, m_WallDistance);
    }

    private void setupTemplate() {
        if(figure) { figure.gameObject.SetActive(false); }

        var random = Random.Range(0, templates.Length);
        figure = templates[random];
        figure.gameObject.SetActive(true);
        // настроить внешний вид фигуры
        figure.SetupRandomFigure();
    }
}