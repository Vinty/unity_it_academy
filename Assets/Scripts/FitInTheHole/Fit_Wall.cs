﻿using System.Collections.Generic;
using FitInTheHole;
using UnityEngine;

public class Fit_Wall{
    private List<Transform> cubes;
    private Transform       parent;

    public Transform Parent => parent;

    public Fit_Wall(int sizeX, int sizeY, GameObject prefab) {
        GenerateWall(sizeX, sizeY, prefab);
    }

    public void SetUpWall(Fit_Template template, float position) {
        parent.transform.position = new Vector3(0f, 0.5f, position);
        foreach (var cube in cubes)
        {
            cube.gameObject.SetActive(true);
        }

        var figure = template.GetFigure();
        for (int f = 0; f < figure.Length; f++)
        {
            for (int c = 0; c < cubes.Count; c++)
            {
                if (Mathf.Abs(figure[f].position.x - cubes[c].position.x) > 0.1f)
                {
                    continue;
                }

                var yDistance = figure[f].position.y - cubes[c].position.y;
                if (Mathf.Abs(yDistance) > 0.1f)
                {
                    continue;
                }
                
                cubes[c].gameObject.SetActive(false);
            }
        }
    }

    private void GenerateWall(int sizeX, int sizeY, GameObject prefab) {
        cubes = new List<Transform>();
        // аналог EmptyGameObject
        parent = new GameObject("Wall").transform;
        for(int x = -sizeX + 1; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {
                Quaternion rotation = Quaternion.identity;
                GameObject obj      = Object.Instantiate(prefab, new Vector3(x, y, 0f), rotation);
                obj.transform.parent = parent;
                cubes.Add(obj.transform);
            }
        }

        parent.position = new Vector3(0f, 0.5f, 0f);
    }
}