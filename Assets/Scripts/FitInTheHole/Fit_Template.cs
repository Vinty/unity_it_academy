﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace FitInTheHole{
    public class Fit_Template : MonoBehaviour{
        [SerializeField] private Transform[] m_Cubes;
        [SerializeField] private Transform m_PlayerPosition;
        [SerializeField] private Transform[] m_PositionVariants;
        
        public Transform CurrentTearget{ get; private set;}

        private FitInTheHole_FigureTweener _tweener;
        private int currentPosition = -1;
        public Transform[] GetFigure()
        {
            var figure = new Transform[m_Cubes.Length + 1];
            m_Cubes.CopyTo(figure, 0);
            figure[figure.Length - 1] = CurrentTearget;
            return figure;
        }

        public void SetupRandomFigure()
        {
            int random = Random.Range(0, m_PositionVariants.Length);
            for (int i = 0; i < m_PositionVariants.Length; i++)
            {
                bool isTarget = i == random;
                m_PositionVariants[i].gameObject.SetActive(isTarget);
                if (isTarget)
                {
                    CurrentTearget = m_PositionVariants[i];
                }
            }
        }

        private void Update()
        {
            if (_tweener)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                MoveLeft();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                MoveRight();
            }
        }

        private void MoveLeft()
        {
            if (!isMovementPosible(1))
            {
                return;
            }

            currentPosition += 1;
            _tweener = m_PlayerPosition.gameObject.AddComponent<FitInTheHole_FigureTweener>();
            _tweener.Tween(m_PlayerPosition.position, m_PositionVariants[currentPosition].position);
        }
        private void MoveRight()
        {
            if (!isMovementPosible(-1))
            {
                return;
            }
            currentPosition -= 1;
            _tweener = m_PlayerPosition.gameObject.AddComponent<FitInTheHole_FigureTweener>();
            _tweener.Tween(m_PlayerPosition.position, m_PositionVariants[currentPosition].position);
        }
//  можно ли туда попасть
        private bool isMovementPosible(int direction)
        {
            var sum = currentPosition + direction;
            return sum >= 0 && sum < m_PositionVariants.Length;
        }
    }
    
       
}