﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FingerDriverPlayer : MonoBehaviour{
    [SerializeField] private FingerDriverTrack m_Track;

    [SerializeField] private FingerDriverInput m_Input;

    //точка по которой будем проверять, вылетела ли машинка за пределы трассы
    [SerializeField] private Transform m_TrackingPoint;

    [SerializeField] private float m_CarSpeed = 1f;
    [SerializeField] private float m_maxSteer = 90f;

    private static bool isSpeedUP;

    public static void SpeedUp() {
        isSpeedUP = true;
    }

    void Update() {
        if(isSpeedUP) {
            isSpeedUP  =  false;
            m_CarSpeed += 1f;
        }

        if(m_Track.IsPointInTrack(m_TrackingPoint.transform.position)) {
            var velocity = Time.deltaTime * m_CarSpeed * transform.up;
            transform.Translate(velocity, Space.World);
            var steer = Time.deltaTime * m_Input.SteerAxis * m_maxSteer;
            transform.Rotate(0f, 0f, steer);
        }
        else {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
