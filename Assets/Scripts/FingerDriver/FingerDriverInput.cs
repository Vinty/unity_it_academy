﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverInput : MonoBehaviour
{
    [SerializeField] private Transform m_SteerWheelTransform;
    [SerializeField] [Range(0f, 180f)] private float m_MaxSteerAngle = 90f;
    [SerializeField] [Range(0f, 1f)] private float m_SteerAcceleration = 0.25f;

    private float steerAxis;
    public float SteerAxis
    {
        get => steerAxis;
        set => steerAxis = Mathf.Lerp(steerAxis, value, m_SteerAcceleration);
    }
    
    private Vector2 startSteerWheelPoint;
    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        //запоминаем координату рулевого колеса в экранной системе координат;
        startSteerWheelPoint = new Vector2(Screen.width * 0.5f, Screen.height * 0.2f);
        //mainCamera.WorldToScreenPoint(m_SteerWheelTransform.position);
        
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            //Измерение угла между рулем и точкой касания
            Vector2 inputDir = (Vector2)Input.mousePosition - startSteerWheelPoint;
            float angle = Vector2.Angle(Vector2.up, inputDir);
            
            //приведение к диапазону от 0 до 1
            angle /= m_MaxSteerAngle;

            if (Input.mousePosition.x > startSteerWheelPoint.x)
            {
                angle *= -1;
            }

            SteerAxis = angle;
        }
        else
        {
            SteerAxis = 0;
        }
        
        m_SteerWheelTransform.localEulerAngles = 
            new Vector3(0f, 0f, SteerAxis * m_MaxSteerAngle);

        Vector3 pos = mainCamera.ScreenToWorldPoint(startSteerWheelPoint);
        pos.z = -1;

        m_SteerWheelTransform.position = pos;

    }
}
