﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverCamera : MonoBehaviour
{
    [SerializeField] private Transform m_carTransform;
    private float cameraZ;
    
    // Start is called before the first frame update
    void Start()
    {
        cameraZ = transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = m_carTransform.position;
        position.z = cameraZ;
        
        transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * 5f);
    }
}
