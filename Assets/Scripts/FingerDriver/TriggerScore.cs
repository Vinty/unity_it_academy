﻿using TMPro;
using UnityEngine;

public class TriggerScore : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Car"))
        {
            ScoreLevel.setScore();
        }
    }
}