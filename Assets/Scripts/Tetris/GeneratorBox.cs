﻿using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneratorBox : MonoBehaviour
{
    [SerializeField] private GameObject text;
    [SerializeField] private GameObject textGameOver;
    [SerializeField] private GameObject textCount;
    [SerializeField] private GameObject gameOverPlane;
    [SerializeField] private GameObject[] respawns;
    [SerializeField] private GameObject triggerLevel01;
    public GameObject lineBlock;
    public GameObject lBlock;
    public GameObject cubeBlock;
    public GameObject zBlock;
    private GameObject _currentBlock;

    private GameObject _cube;
    private GameObject _borderFoot;
    private GameObject _borderTop;
    private GameObject _borderTop2;
    private GameObject _borderLeft;
    private GameObject _borderRight;
    private bool _isCompleteCreateBorder = false;
    private Vector3 _mBorderVertical;
    private Vector3 _horizontal;
    private bool _moveFootComplete = false;
    private bool _moveTopComplete = false;
    private bool _moveLeftComplete = false;
    private bool _moveRightComplete = false;
    private bool _isCurrentBlockAlive = false;
    private GameObject _nextBlock;
    private int _count = 0;
    private bool _isGameOver = false;
   
    private int _countBlockOnLevel01Trigger = 0;

//    private float fall = 0;
    public float fallSpeed = 1;
    public Material matBorder;

    private void Start()
    {
        CreateBorder();
    }

    private void Update()
    {
        if (_isGameOver)
        {
            Thread.Sleep(5000);
            SceneManager.LoadScene("End");
        }

        if (!_isCompleteCreateBorder)
        {
            MoveBorderAndStop();
        }

        if (!_isCompleteCreateBorder) return;

        if (_currentBlock != null)
        {
            if (IsSpeedBlockZero())
            {
                Thread.Sleep(300);
                _isCurrentBlockAlive = false;
            }

            if (_currentBlock.transform.position.y > 8f)
            {
                gameOverPlane.GetComponent<Renderer>().enabled = true;
                textGameOver.GetComponent<Renderer>().enabled = true;
                text.GetComponent<Renderer>().enabled = false;
                _isGameOver = true;
            }
        }

        if (!_isCurrentBlockAlive || _currentBlock == null)
        {
            _currentBlock = Instantiate(_nextBlock);
            _currentBlock.transform.position = new Vector3(0.7f, 7.95f, 0f);
            _count += 1;
            textCount.GetComponent<TextMeshPro>().SetText(_count.ToString());
            var component = _currentBlock.GetComponent<Rigidbody>();
            component.velocity.Set(0f, 0.1f, 0f);
            Destroy(_nextBlock);
            Thread.Sleep(300);
            _nextBlock = GetRandomBlock();
            _nextBlock.transform.position = new Vector3(4.413f, 6.232f, -1.849f);
//                var rig = _currentBlock.GetComponent<Rigidbody>();
//                rig.constraints = RigidbodyConstraints.FreezePositionZ;
//                rig.constraints = RigidbodyConstraints.FreezeRotationX;
//                rig.constraints = RigidbodyConstraints.FreezeRotationY;
            _isCurrentBlockAlive = true;
        }
        else
        {
            CheckUserInput();
        }
    }

    private void CreateBorder()
    {
        _borderFoot = GameObject.CreatePrimitive(PrimitiveType.Cube);
        _borderTop = GameObject.CreatePrimitive(PrimitiveType.Cube);
        _borderTop2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        var borderTopCollider = _borderTop.GetComponent<Collider>();
        borderTopCollider.isTrigger = true;
        _borderLeft = GameObject.CreatePrimitive(PrimitiveType.Cube);
        _borderRight = GameObject.CreatePrimitive(PrimitiveType.Cube);

        _borderFoot.transform.localScale = new Vector3(5.147624f, 0.076541f, 1f);
        _borderTop.transform.localScale = new Vector3(5.147624f, 0.076541f, 1f);
        _borderTop2.transform.localScale = new Vector3(2.7422f, 0.076541f, 1f);
        _borderLeft.transform.localScale = new Vector3(7.660695f, 0.076541f, 1f);
        _borderRight.transform.localScale = new Vector3(7.660695f, 0.076541f, 1f);

        var transformRotation = Quaternion.Euler(0, 0, 90);
        _borderRight.transform.rotation = transformRotation;
        _borderLeft.transform.rotation = transformRotation;

        _borderFoot.transform.position = new Vector3(0.004f, 0.054f, -4.1625f);
        _borderTop.transform.position = new Vector3(0.004f, 7.634f, 7.634f);
        _borderTop2.transform.position = new Vector3(4.45f, 5.82f, 7.634f);
        _borderLeft.transform.position = new Vector3(-4.89f, 3.84f, 0f);
        _borderRight.transform.position = new Vector3(4.89f, 3.84f, 0f);
        SetMaterial(_borderFoot);
        SetMaterial(_borderTop);
        SetMaterial(_borderTop2);
        SetMaterial(_borderLeft);
        SetMaterial(_borderRight);
    }

    private void MoveBorderAndStop()
    {
        if (_isCompleteCreateBorder) return;
        if (_moveFootComplete
            && _moveTopComplete
            && _moveLeftComplete
            && _moveRightComplete)
        {
            _isCompleteCreateBorder = true;
            _nextBlock = GetRandomBlock();
            _nextBlock.transform.position = new Vector3(4.413f, 6.232f, -1.849f);
            text.GetComponent<Renderer>().enabled = true;
        }

        if (_borderFoot.transform.position.z < 0.017f)
        {
            MoveFoot();
        }
        else
        {
            _moveFootComplete = true;
        }

        if (_borderRight.transform.position.x > 2.618f)
        {
            MoveRight();
        }
        else
        {
            _moveRightComplete = true;
        }

        if (_borderLeft.transform.position.x < -2.618f)
        {
            MoveLeft();
        }
        else
        {
            _moveLeftComplete = true;
        }

        if (_borderTop.transform.position.z > 0f)
        {
            MoveTop();
        }

        if (_borderTop2.transform.position.z > -1.8f)
        {
            MoveTop2();
        }
        else
        {
            _moveTopComplete = true;
        }
    }

    private void MoveFoot()
    {
        _borderFoot.transform.Translate(Vector3.forward * Time.deltaTime * 2f);
    }

    private void MoveTop()
    {
        _borderTop.transform.Translate(Vector3.back * Time.deltaTime * 3.3f);
    }

    private void MoveTop2()
    {
        _borderTop2.transform.Translate(Vector3.back * Time.deltaTime * 3.3f);
    }

    private void MoveRight()
    {
        _borderRight.transform.Translate(Vector3.up * Time.deltaTime);
    }

    private void MoveLeft()
    {
        _borderLeft.transform.Translate(Vector3.down * Time.deltaTime);
    }

    private void AddComponentRigibodyToObj(GameObject obj)
    {
        obj.AddComponent<Rigidbody>();
    }

    private void CheckUserInput()
    {
//        if (IsDistanceShort(_currentBlock, _borderFoot, 1f))
//        {
//            _isCurrentBlockAlive = false;
//        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (_currentBlock)
            {
                if (IsDistanceShort(_currentBlock, _borderRight, 3f))
                {
                    _currentBlock.transform.position += new Vector3(0.3f, 0f, 0f);
                }
                else
                {
                    _currentBlock.transform.position += new Vector3(0.4f, 0f, 0f);
                }
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (_currentBlock)
            {
                if (IsDistanceShort(_currentBlock, _borderLeft, 3f))
                {
                    _currentBlock.transform.position += new Vector3(-0.3f, 0f, 0f);
                }
                else
                {
                    _currentBlock.transform.position += new Vector3(-0.4f, 0f, 0f);
                }
            }
        }
//        else if ((Input.GetKeyDown(KeyCode.DownArrow)  || Time.time - fall >= fallSpeed) && _isCurrentBlockAlive == true)
        else if (Input.GetKeyDown(KeyCode.DownArrow) && _isCurrentBlockAlive == true && _currentBlock != null)
        {
            _currentBlock.transform.position += new Vector3(0f, -0.5f, 0f);
//            fall = Time.time;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && _currentBlock != null)
        {
            _currentBlock.transform.Rotate(0, 0, 90);
        }
    }

    private void SetMaterial(GameObject obj)
    {
        Material newMat = Resources.Load("FlatRocks", typeof(Material)) as Material;
        Texture _tex = Resources.Load("Barriers") as Texture;
        Renderer m_Renderer = obj.GetComponent<Renderer>();
        newMat.mainTexture = _tex;
        m_Renderer.material = newMat;
    }

    private bool IsDistanceShort(GameObject one, GameObject two, float limit)
    {
        Vector3 dist = one.transform.position - two.transform.position;
        var result = dist.magnitude;
//        Debug.Log("x = " + result);
        return result <= limit;
    }

    private GameObject GetRandomBlock()
    {
        var range = Random.Range(0, 4);
        switch (range)
        {
            case 0:
                return Instantiate(lineBlock);
            case 1:
                return Instantiate(lBlock);
            case 2:
                return Instantiate(cubeBlock);
            case 3:
                return Instantiate(zBlock);
            default:
                return null;
        }
    }

    private bool IsSpeedBlockZero()
    {
        var component = _currentBlock.GetComponent<Rigidbody>();
        return component.velocity.y.Equals(0);
//        WAIT (5, () => {
//            Debug.Log("5 seconds is lost forever");
//        });    
    }

    /*private void WAIT(float seconds, Action action){
             StartCoroutine(_wait(seconds, action));
         }
         IEnumerator _wait(float time, Action callback){
             yield return new WaitForSeconds(time);
             callback();
         }*/
}