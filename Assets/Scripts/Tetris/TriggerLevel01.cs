﻿using System.Collections.Generic;
using UnityEngine;

public class TriggerLevel01 : MonoBehaviour
{
    private List<Collider> _listColliders = new List<Collider>();

    private void Update()
    {
        var count = 0;
        foreach (var collider in _listColliders)
        {
            count++;
        }

        if (count > 8)
        {
            foreach (var colliderCandidateForDelete in _listColliders)
            {
                Destroy(colliderCandidateForDelete.gameObject);       
            }
            _listColliders = new List<Collider>();;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Destroy") && !_listColliders.Contains(other))
        {
            _listColliders.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (_listColliders.Contains(other))
        {
            _listColliders.Remove(other);
        }
    }
}
